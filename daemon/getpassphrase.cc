#include "daemon.ih"

void Daemon::getPassPhrase()
{
    Tty tty;
    tty.echo(Tty::OFF);

    ifstream in;
    Exception::open(in, "/dev/tty");

    while (true)
    {
        cout << "Enter passphrase: " << flush;
        string passphrase1;
        getline(in, passphrase1);

        if (passphrase1.length() < 10)
            throw Exception{} << 
                    "\n"
                    "Error: passphrases must be at leas 10 characters long";
        
        cout << "\nEnter same passphrase again: " << flush;
        string passphrase2;
        getline(in, passphrase2);
        cout << endl;

        if (passphrase1 == passphrase2)
        {
            // see cron/hmac.cc: the passphrase must be 32 chars long
            while (passphrase1.length() < 32)
                passphrase1 += passphrase1;
            passphrase1.resize(32);
        
            d_cron.setPassPhrase(passphrase1);
            break;
        }

        cout << "Different passphrases. Try again\n\n";
    }

}




