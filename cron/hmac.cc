#include "cron.ih"

string Cron::hmac(string const &passPhrase)
{
    HMacBuf hmacbuf(passPhrase, "aes-256-cbc", "sha256", 1024);

    ostream out(&hmacbuf);
    out << passPhrase << eoi;

    string hash = hmacbuf.hash();

    return hash;
}

