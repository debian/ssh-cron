#ifndef INCLUDED_TOKENS_
#define INCLUDED_TOKENS_

struct Tokens
{
    // Symbolic tokens:
    enum Tokens_
    {
        WS = 257,
        NR,
        ID,
        CHAR,
    };

};

#endif
